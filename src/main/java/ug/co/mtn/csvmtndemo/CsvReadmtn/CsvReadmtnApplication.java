package ug.co.mtn.csvmtndemo.CsvReadmtn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CsvReadmtnApplication {

	public static void main(String[] args) {
		SpringApplication.run(CsvReadmtnApplication.class, args);
	}

}
